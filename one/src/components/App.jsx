import { React, useState } from 'react'
import './../css/style.css'
import { Sidebar } from './Sidebar'
import { Board } from './Board'
import { handleLocalStorage } from '../tools/handleLocalStorage'
import { ID_STORAGE } from '../common'

function App() {

  const initialStoredValue = {
    'index': 0,
    'titles': ['A'],
    'tasks': {}
  }

  const [storedValue, setColumnTasks, setLocalStorage] = handleLocalStorage(ID_STORAGE, initialStoredValue)

  const resetStoredValue = () => {
    setLocalStorage(initialStoredValue)
    window.location.reload()
  }

  const updateColumnTasks = (title, tasks, lastIndex = 0) => {
    setColumnTasks(title, [...tasks], lastIndex)
  }

  return (
    <>
      <header>
        <h1>Kato</h1>
      </header>
      <main>
        <Board storedValue={storedValue} updateColumnTasks={updateColumnTasks}/>
        <Sidebar resetStoredValue={resetStoredValue}/>
      </main>
    </>
  )
}

export default App
