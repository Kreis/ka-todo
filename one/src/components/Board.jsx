import React, { useState } from 'react'
import {ColTask} from './colTask'
import '../tools/handleLocalStorage.js'
import './../css/board.css'

export function Board({storedValue, updateColumnTasks}) {
  const indexGenerator = () => {
    storedValue['index'] = storedValue['index'] < 99999999999 ? storedValue['index'] + 1 : 1
    return storedValue['index']
  }

  return (
    <section className="mainBoard">
      {
        storedValue['titles'].map((title) => <ColTask
          key={title}
          title={title}
          indexGenerator={indexGenerator}
          updateColumnTasks={updateColumnTasks}
          tasks={storedValue['tasks'][title]}
          />)
      }
    </section>
  ) 
}
