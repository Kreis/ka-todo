import React, { useState } from 'react'
import './../css/sidebar.css'

export function Sidebar({resetStoredValue}) {
  const [isOpen, setIsOpen] = useState(false);
  const toggleSidebar = () => {
    setIsOpen(!isOpen)
  }

  return (
    <div>
      <div className={`sidebar ${ isOpen ? 'open' : ''}`}>
        <h2>hola</h2>
        <button className='genericButton' onClick={resetStoredValue}></button>
      </div>
      <button className={`sidebarButton ${ isOpen ? 'open' : '' }`} onClick={toggleSidebar}>
      </button>
    </div>
  ) 
}
