import React, { useState } from 'react'
import {RowTask} from './rowTask'
import {RowTaskFirst} from './rowTaskFirst'
import {RowTitle} from './rowTitle'
import './../css/colTask.css'

export function ColTask({title, indexGenerator, updateColumnTasks, tasks}) {

  const [rowList, setRowList] = useState(tasks || [])

  const pushRow = (text) => {
    const newRowList = [...rowList]
    const newIndex = indexGenerator()
    newRowList.push([text, newIndex])
    setRowList(newRowList)
    updateColumnTasks(title, newRowList, newIndex)
  }

  const deleteTask = (id) => {
    const newRowList = [...rowList].filter((row) => row[1] != id)
    setRowList(newRowList)
    updateColumnTasks(title, newRowList)
  }

  return (
    <section className="colTask">
      <RowTitle/>
      <RowTaskFirst pushRow={pushRow}/>
      {
        [...rowList].reverse().map(([text, index]) => 
          <RowTask key={index} id={index} text={text} deleteTask={deleteTask}/>
        )
      }
    </section>
  )
}
