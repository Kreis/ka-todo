import {React, useState} from 'react'
import {useFlexibleArea} from './../customHooks/useFlexibleArea.js'
import './../css/rowTask.css'

export function RowTask({ id, text, deleteTask }) {
  const [rowRef, handleChange] = useFlexibleArea()

  const handleBlur = (event) => {
    if (event.key === 'Enter' && ! event.shiftKey) {
      event.target.blur();
    }
  }

  return (
    <div className='rowTask'>
      <textarea
        rows={1}
        ref={rowRef}
        onChange={handleChange}
        className="rowTaskArea"
        defaultValue={text}
        onKeyDown={handleBlur}
        spellCheck={false}
      />
      <button className="deleteButton" onClick={() => deleteTask(id) }/>
    </div>
  )
}