import {React} from 'react'
import {useFlexibleArea} from './../customHooks/useFlexibleArea.js'
import './../css/rowTask.css'

export function RowTaskFirst({ pushRow }) {
  const [rowRef, handleChange] = useFlexibleArea()


  const handleEnter = (event) => {
    if (event.key === 'Enter' && ! event.shiftKey) {
      event.preventDefault()
      pushRow(event.target.value)
      event.target.value = ""
    }
  }

  return (
      <textarea
        rows={1}
        ref={rowRef}
        onChange={handleChange}
        className="rowTaskArea disable-syntax-corrector"
        onKeyDown={handleEnter}
        spellCheck={false}
        placeholder="Type your task"
      />
  )
}
