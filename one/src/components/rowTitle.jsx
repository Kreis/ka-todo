import {React, useState, useEffect, useRef} from 'react'
import './../css/rowTitle.css'
import {useFlexibleArea} from './../customHooks/useFlexibleArea.js'

export function RowTitle() {
  const [rowRef, handleChange] = useFlexibleArea()
  return (
      <textarea
        rows={1}
        ref={rowRef}
        onChange={handleChange}
        className="rowTitle"
        placeholder="set title"
        spellCheck={false}
      />
  )
}
