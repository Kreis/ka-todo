import { useState, useEffect, useRef } from "react"

export const useFlexibleArea = () => {
  const rowRef = useRef(null)
  
  const [rowValue, setRowValue] = useState("")
  useEffect(() => {
    if (rowRef.current) {
      rowRef.current.style.height = "auto";
      rowRef.current.style.height = `${rowRef.current.scrollHeight}px`
    }
  }, [rowValue])

  const handleChange = (element) => {
    setRowValue(element.target.value)
  }

  return [rowRef, handleChange]
}
