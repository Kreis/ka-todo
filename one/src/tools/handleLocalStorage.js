
export const handleLocalStorage = (key, initialValue) => {

  const stored = window.localStorage.getItem(key)
  const currentStoreValue = stored ? JSON.parse(stored) : {...initialValue}
  
  window.localStorage.setItem(key, JSON.stringify(currentStoreValue))

  const setColumnTasks = (title, newValue, newIndex = 0) => {
    currentStoreValue['tasks'][title] = newValue
    if (newIndex > 0) {
      currentStoreValue['index'] = newIndex
    }
    window.localStorage.setItem(key, JSON.stringify(currentStoreValue))
  }

  const setLocalStorage = (givenValue) => {
    window.localStorage.setItem(key, JSON.stringify(givenValue))
  }

  return [currentStoreValue, setColumnTasks, setLocalStorage]
}